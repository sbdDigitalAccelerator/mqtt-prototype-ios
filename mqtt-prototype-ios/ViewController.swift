//
//  ViewController.swift
//  mqtt-prototype-ios
//
//  Created by Weston Wieser on 9/6/18.
//  Copyright © 2018 Weston Wieser. All rights reserved.
//

import UIKit
import AWSIoT

class ViewController: UIViewController {
    
    
    @IBOutlet weak var messageOutput: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let defaults = UserDefaults.standard
        
        let csrDictionary = [ "commonName": "CertificateSigningRequestCommonName", "countryName":"CertificateSigningRequestCountryName", "organizationName":"CertificateSigningRequestOrganizationName", "organizationalUnitName":"CertificateSigningRequestOrganizationalUnitName" ]
        
        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:d34658a7-683f-4dc6-8996-9c25bb562c7d")
        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let iotManager = AWSIoTManager.default()
        let iot = AWSIoT.default()
        
        AWSIoTDataManager.register(with: configuration!, forKey: "USEast1IoTDataManager")
        
        let iotDataManager = AWSIoTDataManager(forKey: "USEast1IoTDataManager")
        
        
        iotManager?.createKeysAndCertificate(fromCsr: csrDictionary, callback: { (response) -> Void in
            if (response != nil){
                defaults.set(response?.certificateId, forKey:"certificateId")
                defaults.set(response?.certificateArn, forKey:"certificateArn")
                let certificateId = response?.certificateId
                print("response: [\(String(describing: response))]")
                
                let attachPrincipalPolicyRequest = AWSIoTAttachPrincipalPolicyRequest()
                attachPrincipalPolicyRequest?.policyName = "mew-3-messaging"
                attachPrincipalPolicyRequest?.principal = response?.certificateArn
                
                iot.attachPrincipalPolicy(attachPrincipalPolicyRequest!).continueWith (block: { (task) -> AnyObject? in
                    if let error = task.error {
                        print("failed: [\(error)]")
                    }
                    print("result: [\(String(describing: task.result))]")
                    
                    // Connect to the AWS IoT platform
                    if (task.error == nil)
                    {
                        
                        iotDataManager.connect( withClientId: UUID().uuidString, cleanSession:true, certificateId:certificateId!, statusCallback: self.mqttEventCallback)
                        
                        iotDataManager.subscribe(toTopic: "myTopic", qoS: .messageDeliveryAttemptedAtMostOnce, messageCallback: { [weak self]
                            (payload) -> Void in
                            DispatchQueue.main.async {
                                let stringValue = NSString(data: payload, encoding: String.Encoding.utf8.rawValue)!
                                
                                if let weakSelf = self {
                                    weakSelf.messageOutput.text = weakSelf.messageOutput.text + "\(Date().description): \(stringValue)\n"
                                }
                            }
                        })
                        
                    }
                    return nil
                })
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func mqttEventCallback( _ status: AWSIoTMQTTStatus )
    {
        print("connection status = \(status.rawValue)")
    }
}

